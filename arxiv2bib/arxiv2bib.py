# Copyright (C) 2017 Francesco Montanari

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

"""arxiv2bib: Parse arxiv numbers and download the respective bibtex entries

Provide functions to parse arxiv numbers text files. Query inspire.net
to print to stdout the bibtex entries corresponding to the respective
arxiv numbers. All the results are printed to stdout.

.. note::

   This program works with both Python 2 and 3. If the input file
   contains Unicode characters, Python 3 is recommended.
"""


import argparse
import re
import sys

from . import __version__
from .pyinspire.pyinspire import get_text_from_inspire


def get_arxiv_ids(string, prefix='arxiv:'):
    """Read string and return a list containing arxiv numbers. Match
    patterns such as `arXiv:1234.5678` or `arXiv:gr-qc/1234567`.
    """
    regexp = r'[A-Za-z0-9.\-\/]*'
    arxiv_ids = re.findall(prefix+regexp, string, re.IGNORECASE)

    # Use slices to remove the prefix (it could appear in different
    # case combinations).
    return [arxiv_id[len(prefix):] for arxiv_id in arxiv_ids]


def _check_size(size, threshold=100):
    """Exit if the size is larger than the threshold."""
    if size > threshold:
        msg = ("Error: too large reference list ({} items). \n"
               "Provide a smaller list (less than {} items), "
               "or run the program with "
               "the -f flag.\n".format(size, threshold))
        sys.stderr.write(msg)
        sys.exit(1)


def get_bibtex(myfile, force=False):
    """Print to stdout the inspire query result based on arxiv numbers
    listed in myfile.

    """
    with open(myfile, 'r') as f:
        string = f.read()

    prefix = 'arxiv:'
    arxiv_ids = get_arxiv_ids(string, prefix=prefix)
    arxiv_ids = set(arxiv_ids)  # Remove duplicates.
    size = len(arxiv_ids)

    if not force:
        _check_size(size)

    base = 'find eprint '
    search = base + " or eprint ".join(sorted(arxiv_ids))

    resultformat = 'bibtex'
    tags = None

    if size > 0:
        result = get_text_from_inspire(search=search,
                                       resultformat=resultformat,
                                       ot=tags)
        sys.stdout.write(result)

    sys.stdout.write("%%% arxiv2bib retrieved {} references."
                     .format(len(arxiv_ids)))
    sys.exit(0)


def _get_parser():
    """Return the parser."""
    parser = argparse.ArgumentParser(
        description=("Parse arxiv numbers and download the "
                     "respective bibtex entries"))

    parser.add_argument('--version', action='version',
                        version='%(prog)s '+__version__)

    parser.add_argument('FILE', metavar=('FILE'),
                        type=str, nargs=1,
                        help=('FILE containing arxiv numbers (examples: '
                              'arXiv:1234.5678, arXiv:gr-qc/1234567)'))

    parser.add_argument('-f', '--force', action="store_true",
                        help='force download of large lists')

    return parser


def get_cli():
    """Command line interface"""
    parser = _get_parser()
    args = parser.parse_args()
    get_bibtex(args.FILE[0], args.force)


if __name__ == '__main__':
    get_cli()
