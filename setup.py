#!/usr/bin/env python3

from setuptools import setup, find_packages
import arxiv2bib

setup(name='arxiv2bib',
      version=arxiv2bib.__version__,
      description='Parse arxiv numbers and download the respective bibtex entries',
      classifiers=[
          "Development Status :: 4 - Beta",
          "Environment :: Console",
          "Intended Audience :: Developers",
          "Programming Language :: Python :: 2.7",
          "Programming Language :: Python :: 3",
          "Programming Language :: Python :: 3.2",
          "Programming Language :: Python :: 3.3",
          "Programming Language :: Python :: 3.4",
          "Programming Language :: Python :: 3.5",
      ],
      author='Francesco Montanari',
      author_email='fmnt@fmnt.info',
      url='https://gitlab.com/montanari/arxiv2bib',
      license='GPL3+',
      packages=find_packages(),
      entry_points={'console_scripts':
                    ['arxiv2bib = arxiv2bib.arxiv2bib:get_cli',]}
)
